<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PostController::class, 'welcome']);

//route to return a view where the user can create a post
Route::get('/posts/create', [PostController::class, 'create']);

//route for a route wherien form data can be sent via POST method
Route::post('/posts', [PostController::class, 'store']);

//route that will return a view containing all posts
Route::get('/posts', [PostController::class, 'index']);

//route that will return a view containing only the authenticated user's posts
Route::get('/posts/myPosts', [PostController::class, 'myPosts']);

//route that will show a specific posts' view based on the URL parameter's id
Route::get('/posts/{id}', [PostController::class, 'show']);

//route that will show a form to edit a post based on the URL parameter's id
Route::get('posts/{id}/edit', [PostController::class, 'edit']);

Auth::routes();

Route::get('/home', [HomeController::class, 'index']);